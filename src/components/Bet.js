import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		margin: theme.spacing(1)
	},
	paper: {
		padding: theme.spacing(2),
		maxWidth: 700,
		backgroundColor: 'lightgrey'
	},
	image: {
		width: 128,
		height: 128,
	},

}));

export default function Bet(props) {
	const classes = useStyles();

	const [betId, setBetId] = useState('');

	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {


		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleAcceptBet = async () => {

		console.log(props.bet.id);
		let bet = {
			id: props.bet.id,
		};

		const response = await fetch(`https://localhost:8100/acceptbet/${bet.id}`);

		console.log(await response.json());
		handleClose();
		props.fetchRequest();
	}

	return (
		<div className={classes.root}>
			<Paper className={classes.paper}>
				<Grid container spacing={2}>
					<Grid item xs={12} sm container>
						<Grid item xs container direction="column" spacing={2}>
							<Grid item xs>
								<Typography gutterBottom variant="subtitle1">
									Автор предложения: {props.bet.authorId}
								</Typography>
								<Typography variant="body2" gutterBottom>
									Комментарий: {props.bet.description}
								</Typography>
								<Typography variant="body2" color="textSecondary">
									Дата создания: {props.bet.creationDate}
								</Typography>
								<Typography variant="body2" color="textSecondary">
									Id: {props.bet.id}
								</Typography>
							</Grid>
							<Grid item>
								<Button size="small" color="primary" variant="contained" onClick={handleClickOpen}>Принять</Button>
								<Dialog
									open={open}
									onClose={handleClose}
									aria-labelledby="alert-dialog-title"
									aria-describedby="alert-dialog-description"
								>
									<DialogTitle id="alert-dialog-title">Подтверждение</DialogTitle>
									<DialogContent>
										<DialogContentText id="alert-dialog-description">
											Вы уверены, что хотите принять ставку?
                    					</DialogContentText>
									</DialogContent>
									<DialogActions>
										<Button onClick={handleClose} color="primary">
											Отмена
                    					</Button>
										<Button onClick={handleAcceptBet} color="primary" autoFocus>
											Принять
                    					</Button>
									</DialogActions>
								</Dialog>
							</Grid>
						</Grid>
						<Grid item>
							<Typography variant="h6">{props.bet.price} руб.</Typography>
						</Grid>
					</Grid>
				</Grid>
			</Paper>
		</div>
	);
}


