import React, { useEffect, useState } from 'react';
import { Link as MaterialLink } from '@material-ui/core';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import SchoolIcon from '@material-ui/icons/School';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { useCookies } from 'react-cookie';
import { Redirect } from 'react-router';
import jwt_decode from "jwt-decode";

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
}));

function CustomNav(props) {

	const navStyle = {
		color: 'white',
		marginRight: '20px',
		textDecoration: 'none'
	}
	const [cookies, setCookie, removeCookie] = useCookies(['name']);
	const [jwt, setJwtToken, removeJwtToken] = useCookies(['jwt_jumping_token']);
	const [redirect, setRedirect] = useState(false);



	const logout = async (e) => {
		e.preventDefault();

		removeCookie("name");
		removeJwtToken("jwt_jumping_token");
		setRedirect(true);


	}

	const classes = useStyles();

	if (redirect) {
		return <Redirect to="/" />
	}


	return (

		<AppBar position="static" color="primary">
			<Toolbar>
				<IconButton edge="start" color="inherit" aria-label="menu" component={Link} to="/">
					<SchoolIcon />
				</IconButton>
				<Typography variant="h6" className={classes.title}>
					{props.name ? 'Привет, ' + props.name : 'JUMPING'}
				</Typography>
				<MaterialLink variant="h6" style={navStyle} component={Link} to="/">Главная</MaterialLink>				
				{props.role === "Student" &&
					<MaterialLink variant="h6" style={navStyle} component={Link} to="/StudentPersonalArea">Личный кабинет</MaterialLink>
				}
				{props.role === "Teacher" &&
					<MaterialLink variant="h6" style={navStyle} component={Link} to="/TeacherPersonalArea">Личный кабинет</MaterialLink>
				}
				<MaterialLink variant="h6" style={navStyle} component={Link} to="/catalog">КАТАЛОГ</MaterialLink>
				<MaterialLink variant="h6" style={navStyle} component={Link} to="/requests">ЗАЯВКИ</MaterialLink>
				<MaterialLink variant="h6" style={navStyle} component={Link} to="/lessons">УРОКИ</MaterialLink>
				{props.name ?
					<MaterialLink variant="h6" style={navStyle} component={Link} onClick={logout}>Выйти</MaterialLink>
					:
					<MaterialLink variant="h6" style={navStyle} component={Link} to="/SignInSide">ВОЙТИ</MaterialLink>
				}
			</Toolbar>
		</AppBar>
	);
}

export default CustomNav;
