import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { Link as MaterialLink } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
	leftSideButtons: {
		fontSize: "16px",
    color: "grey"
	},
}));


function TeacherSideBar() {
  const classes = useStyles();
  return (
  <div>
    <ListItem button>
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <MaterialLink 
        variant="h6"
        className={classes.leftSideButtons}
        component={Link} 
        to="/TeacherProfile">Мой профиль</MaterialLink>
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <ShoppingCartIcon />
      </ListItemIcon>
      <MaterialLink 
      variant="h6" 
      className={classes.leftSideButtons}
      component={Link} 
      to="/TeacherRequests">Мои заявки</MaterialLink>
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <MaterialLink 
      variant="h6" 
      className={classes.leftSideButtons}
      component={Link} 
      to="/TeacherLessons">Мои уроки</MaterialLink>
    </ListItem>
  </div>
  );
}

export default TeacherSideBar;