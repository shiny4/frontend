import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Button, Divider } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: theme.spacing(1)
    },
    paper: {
        padding: theme.spacing(2),
        maxWidth: 700,
        backgroundColor: 'lightgrey'
    },
    image: {
        width: 128,
        height: 128,
    },

}));

export default function Teacher(props) {
    const classes = useStyles();
    const userRole = useSelector(state => state.userRole);

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    Имя: {props.teacher.name}
                                </Typography>
                                <Typography gutterBottom variant="subtitle1">
                                    Фамилия: {props.teacher.familyName}
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    email: {props.teacher.email}
                                </Typography>
                                <Typography variant="body2" color="textSecondary">
                                    Рейтинг: {props.teacher.ratingAsTeacher}
                                </Typography>                             

                            </Grid>
                            {userRole === "Student" &&
                                <Grid item xs={4} container direction="column" spacing={2}>
                                    <Button color="Primary" size="small" variant="contained">Подать заявку</Button>
                                </Grid>
                            }
                        </Grid>

                    </Grid>
                </Grid>
            </Paper>
        </div>
    );
}


