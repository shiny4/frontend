import React, { useState, useEffect } from 'react';

import Home from './pages/home';
import Profile from './pages/profile';
import Requests from './pages/requests';
import RequestDetails from './pages/requestDetails';
import Lessons from './pages/lessons';
import StudentPersonalArea from './pages/StudentPersonalArea';
import Grid from '@material-ui/core/Grid';
import CustomNav from './components/CustomNav'
import SignInSide from './pages/SignInSide';
import SignUp from './pages/SignUp';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { useCookies, withCookies, Cookies } from 'react-cookie';
import StudentLessons from './pages/StudentProfile/StudentLessons';
import StudentProfile from './pages/StudentProfile/StudentProfile';
import StudentRequests from './pages/StudentProfile/StudentRequests';
import Catalog from './pages/Catalog';
import NotFound from './pages/NotFound';
import jwt_decode from "jwt-decode";
import TeacherProfile from './pages/TeacherProfile/TeacherProfile';
import TeacherLessons from './pages/TeacherProfile/TeacherLessons';
import TeacherRequests from './pages/TeacherProfile/TeaherRequests';
import TeacherPersonalArea from './pages/TeacherPersonalArea';
import { useSelector, useDispatch } from 'react-redux';
import { setJwtToken, resetJwtToken, setUserId, resetUserId, setUserRole, resetUserRole } from './Redux/actions/jwtActions';

function App() {
	const [name, setName] = useState('');
	const [userGuid, setUserGuid] = useState('');
	const [cookies, setCookie] = useCookies(['name']);
	const [jwt, setJwtToken2] = useCookies(['jwt_jumping_token']);
	const dispatch = useDispatch();
	const userRole = useSelector(state => state.userRole);

	useEffect(() => {
		(
			async () => {				
				setName(cookies.name);
				
				if (Object.keys(jwt).length > 0)
				{
					var decoded = jwt_decode(jwt.jwt_jumping_token);
					var role = jwt_decode(jwt.jwt_jumping_token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
					var userId = jwt_decode(jwt.jwt_jumping_token)["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid"];

					setUserGuid(userId);
					dispatch(setJwtToken(jwt.jwt_jumping_token));
					dispatch(setUserId(userId));
					dispatch(setUserRole(role));
				}
			}
		)();
	});

	return (

		<div className="App">
			<Router>
				<CustomNav name={name} setName={setName} role={userRole}/>
				<Switch>
					<Route path="/" exact component={Home} />
					<Route path="/catalog" exact component={Catalog} />
					<Route path="/profile" component={Profile} />
					{userRole === "Student" && 
						<Route path="/StudentPersonalArea" component={StudentPersonalArea} />
					}
					{userRole === "Student" &&
						<Route path="/StudentProfile" component={StudentProfile} />
					}
					{userRole === "Student" &&
						<Route path="/StudentLessons" component={StudentLessons} />
					}
					{userRole === "Student" &&
						<Route path="/StudentRequests" component={StudentRequests} />
					}
					
					{userRole === "Teacher" && 
						<Route path="/TeacherPersonalArea" component={TeacherPersonalArea} />
					}
					{userRole === "Teacher" &&
						<Route path="/TeacherProfile" component={TeacherProfile} />
					}
					{userRole === "Teacher" &&
						<Route path="/TeacherLessons" component={TeacherLessons} />
					}
					{userRole === "Teacher" &&
						<Route path="/TeacherRequests" component={TeacherRequests} />
					} 
					<Route path="/requests" component={Requests} />
					<Route path="/lessons" component={Lessons} />
					<Route path="/SignInSide" component={() => <SignInSide setName={setName} />} />
					<Route path="/SignUp" component={SignUp} />
					<Route path="/request/:id" component={RequestDetails} />

				</Switch>
			</Router>
		</div>

	);
}

export default App;
