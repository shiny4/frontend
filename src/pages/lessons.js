import { Container, Divider } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { LessonServiceAPI } from '../API/API';
import Lesson from '../components/Lesson';
import { useSelector, useDispatch } from 'react-redux';

function Lessons() {
  const [allLessons, setAllLessons] = useState([]);
  const jwt = useSelector(state => state.jwt);
  const userId = useSelector(state => state.userId);
  const userRole = useSelector(state => state.userRole);

  useEffect(() => {
    getAllLessons();
  }, []);

  const getAllLessons = async () => {
    const response = await LessonServiceAPI.get('/api/v1/Lessons');
    var content = await response.data.content;
    setAllLessons(content);
  }

  return (  
    <>
      <Container maxWidth="sm">
        {allLessons.map(lesson => (
          <Lesson lesson={lesson} />
        ))}
      </Container>
    </>
  );
};

export default Lessons;