import React, {useEffect, useState} from 'react';
import { Link as MaterialLink} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { SignalWifi1BarLockSharp } from '@material-ui/icons';
import ReceiptIcon from '@material-ui/icons/Receipt';
import Container from '@material-ui/core/Container';
import { ApplicationServiceAPI } from '../API/API';

const useStyles = makeStyles({
    root: {
      maxWidth: 500,
      marginBottom: '20px',

    },
  });

function Requests(){
    const classes = useStyles();

    useEffect(() => {
        fetchRequests();
    },[]);

    const [requests, setRequests] = useState([]);

    const fetchRequests = async () => {
        const response = await ApplicationServiceAPI('/api/CrudRequest');
        setRequests(response.data);
    }
    

    return (
        <Container maxWidth="sm">
            {requests.map(request =>(
                
                <Card className={classes.root}>
                <CardActionArea>
                  
                  <CardContent>

                    <Typography gutterBottom variant="h6" component="h5">
                    <ReceiptIcon fontSize="small"/> {request.studentId}
                    </Typography>

                    <Typography variant="body1" component="p">
                    	Преподаватель: {request.teacherId}
                    </Typography>

                    <Typography variant="body2" color="textSecondary" component="p">
                      {request.description}
                    </Typography>

					<Typography variant="h3" color="textPrimary" component="h3">
                      {request.price} руб.
                    </Typography>

                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <Button size="small" color="primary">
                    <MaterialLink component={ Link } to={`/request/${request.id}`}>Просмотреть</MaterialLink>
                  </Button>
                  <Button size="small" color="primary">
                    Learn More
                  </Button>
                </CardActions>
              </Card>
                
                
            ))}
        </Container>
    );
};

export default Requests;