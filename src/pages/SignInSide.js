import React, { useState } from 'react';
import { Link as MaterialLink } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import studyImage from '../static/images/study.jpeg';
import { Redirect } from 'react-router';
import jwt_decode from "jwt-decode";
import { useCookies } from 'react-cookie';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      Jumping
      {' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `url(${studyImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignInSide(props) {
  const classes = useStyles();

  const state = {
    button: 1
  };

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [redirect, setRedirect] = useState(false);
  const [cookies, setCookie] = useCookies(['name']);
  const [jwt, setJwtToken] = useCookies(['jwt_jumping_token']);

  const onSubmit = async (e) => {
    e.preventDefault();
    if (state.button === 1) {
      const response = await fetch('http://localhost:8100/api/v1/auth/authentication', {
        method: 'POST',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        body: JSON.stringify({
          email,
          password,
          "role": "Teacher"
        }),
      });
      if (response.ok) {

        const content = await response.json();
        var decoded = jwt_decode(content.data);
        //console.log(decoded);
        //console.log(decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]);
        console.log(content);
        setRedirect(true);
        setCookie('name', content.messageCode);
        setJwtToken('jwt_jumping_token', content.data);
        props.setName(cookies.name);
      } else {
        alert("Ошибка HTTP: " + response.status);
      }

    }
    if (state.button === 2) {
      const response = await fetch('http://localhost:8100/api/v1/auth/authentication', {
        method: 'POST',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        body: JSON.stringify({
          email,
          password,
          "role": "Student"
        }),
      });
      if (response.ok) {

        const content = await response.json();

        var decoded = jwt_decode(content.data);
        //console.log(decoded);
        //console.log(decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]);
        console.log(content);
        setRedirect(true);
        setCookie('name', content.messageCode);
        setJwtToken('jwt_jumping_token', content.data);
        props.setName(cookies.name);
      } else {
        alert("Ошибка HTTP: " + response.status);
      }

    }
  };

  if (redirect) {
    return <Redirect to="/" />
  }

  const SubmitAsTeaher = () => {
    // console.log("SubmitAsTeaher");
    // console.log({
    //   email,
    //   password
    // });



    //const content = await response.json();
    //console.log(content);
  }

  const SubmitAsStudent = () => {
    console.log("SubmitAsStudent");
    console.log({
      email,
      password
    });
  }


  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} >

      </ Grid>
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            ВХОД
          </Typography>
          <form onSubmit={onSubmit} className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Электронная почта"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={e => setEmail(e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Пароль"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={e => setPassword(e.target.value)}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Запомнить меня"
            />
            <Button
              onClick={() => (state.button = 1)}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Войти как преподаватель
            </Button>
            <Button
              onClick={() => (state.button = 2)}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Войти как ученик
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Забыли пароль?
                </Link>
              </Grid>
              <Grid item>
                <MaterialLink component={Link} to="/SignUp" variant="body2">
                  Регистрация
                </MaterialLink>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}