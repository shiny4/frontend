import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import Form from '@material-ui/core/FormControl';
import Box from '@material-ui/core/Box';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Divider, Hidden, Link as MaterialLink } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Bet from '../components/Bet';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';


const useStyles = makeStyles({
	root: {
		maxWidth: 900,
		marginBottom: '20px',

	},
	container: {
		marginTop: '20px'
	}
});

function RequestDetails({ match }) {
	const classes = useStyles();

	const [open, setOpen] = React.useState(false);
	const [betdescription, setBetDescription] = useState('');
	const [betprice, setBetPrice] = useState(0);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const Submit = async (e) => {
		e.preventDefault();

		const response = await fetch('http://localhost:8100/api/bet', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				"requestId": request.id,
				"authorId": "1c7dab91-3317-41a5-adf5-8682fa3d1299",
				"Description": betdescription,
				"Price": betprice
			})
		});

		setOpen(false);
		fetchRequest();
	};

	useEffect(() => {
		fetchRequest();
	}, []);

	const [request, setRequest] = useState({});
	const [bets, setBets] = useState([]);

	const fetchRequest = async () => {
		axios.get(`http://localhost:8100/api/v1/UseCaseRequest/GetRequestWithDetailedInfo/${match.params.id}`)
			.then(res => {
				const request = res.data;
				let bets = request.bets;
				setRequest(request);
				setBets(bets);
			});
	}

	return (
		<Container maxWidth="md" className={classes.container}>
			<Card className={classes.root}>

				<CardContent>
					<Typography gutterBottom variant="h6" component="h5">
						{request.studentId}
					</Typography>
					<Typography variant="body2" color="textSecondary" component="p">
						Комментарий: {request.description}
						<Typography variant="h4" color="textPrimary" component="h3">
							Цена: {request.price}
						</Typography>

						{request.status == 0 && (
							<Typography variant="h6" component="h3">
								Статус: Активна
							</Typography>
						)}

						{request.status == 1 && (
							<Typography variant="h6" color="textPrimary" component="h3">
								Статус: Закрыта
							</Typography>
						)}

					</Typography>
				</CardContent>
				<Divider />
				<Box margin={2}>
					<Button size="small" color="secondary" variant="contained" onClick={handleClickOpen}>Сделать встречное предложение</Button>

					<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
						<form onSubmit={Submit}>
							<DialogTitle id="form-dialog-title">Новое предложение</DialogTitle>
							<DialogContent>

								<TextField
									autoFocus
									margin="dense"
									id="Description"
									label="Описание"
									type="text"
									fullWidth
									onChange={e => setBetDescription(e.target.value)}
								/>

								<TextField
									margin="dense"
									id="price"
									label="Цена"
									type="text"
									fullWidth
									onChange={e => setBetPrice(e.target.value)}
								/>

							</DialogContent>
							<DialogActions>
								<Button onClick={handleClose} color="primary">
									Отмена
						</Button>
								<Button type="submit" color="primary">
									Создать
						</Button>
							</DialogActions>
						</form>
					</Dialog>

				</Box>

				<Divider />
				<CardContent>

					{bets.map(bet => (
						<Bet bet={bet} fetchRequest={fetchRequest} />
					))}
				</CardContent>
			</Card>
		</Container>
	);
};

export default RequestDetails;