import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import studyImage from '../static/images/study.jpeg';
import { Redirect } from 'react-router';
import { useCookies } from 'react-cookie';
import jwt_decode from "jwt-decode";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
        Jumping
      {' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `url(${studyImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUp(props) {
  const classes = useStyles();

  const state = {
    button: 1
  };

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [redirect, setRedirect] = useState(false);
  const [cookies, setCookie] = useState('');
  const [jwt, setJwtToken] = useState('');

  const onSubmit = async (e) => {
    e.preventDefault();

    
    
    // const response = await fetch('http://localhost:8100/api/register', {
    //     method: 'POST',
    //     headers: {'Content-Type': 'application/json'},
    //     body: JSON.stringify({
    //         name,
    //         email,
    //         password
    //     })
    // });

    const response = await fetch('http://localhost:8100/api/v1/auth/registration', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
          "email": email,
          "password": password,
          "repeatPassword": password
        }),
    });
    if (response.ok) { // если HTTP-статус в диапазоне 200-299 получаем тело ответа 
      let content = await response.json();
      console.log(content);
      alert("Поздравляем, вы зарегистрировались на нашем сайте. Тепрь введите свой логин и пароль, чтобы войти на наш сайт как зарегистрированный пользователь.");
      setRedirect(true);
      // setCookie('name', content.messageCode);
      // setJwtToken('jwt_jumping_token', content.data);
      // props.setName(cookies.name);
    } else {
      alert("Ошибка HTTP: " + response.status);
    }    
    
    //setRedirect(true);
  };

  if(redirect){
    return <Redirect to="/SignInSide" />
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} >
       
      </ Grid>
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            РЕГИСТРАЦИЯ
          </Typography>
          <form onSubmit={onSubmit} className={classes.form} noValidate>

          <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="Name"
              label="Логин"
              name="Name"
              autoFocus
              onChange={e => setName(e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Электронная почта"
              name="email"
              autoComplete="email"
              onChange={e => setEmail(e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Пароль"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={e => setPassword(e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Зарегистрироваться
            </Button>

            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}