import React, { useState, useEffect, useRef, Component } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import TeacherSideBar from '../../components/TeacherSideBar';
import { LessonServiceAPI } from '../../API/API';
import { useSelector, useDispatch } from 'react-redux';
import Lesson from '../../components/Lesson';
import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import CustomizedSnackbars from '../../components/CustomizedSnackbars';
import { HighlightOff, PlayArrow } from '@material-ui/icons';
import StopIcon from '@material-ui/icons/Stop';


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  buttonContainer: {
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(6)
  }
}));

export default function TeacherLessons() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const userId = useSelector(state => state.userId);
  const [openDialog, setOpenDialog] = React.useState(false);
  const handleClickOpen = () => {
    setOpenDialog(true);
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const [teacherLessons, setTeacherLessons] = useState([]);

  useEffect(() => {
    getTeacherLessons();
  }, []);
  const [openSnack, setOpenSnack] = React.useState(false);

  const handleSnackClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenSnack(false);
  };

  const getTeacherLessons = async () => {
    const response = await LessonServiceAPI.get('/api/v1/Lessons/byteacher/' + userId);
    var content = await response.data.content;
    setTeacherLessons(content);
  }



  const lessonTitle = useRef('');
  const lessonDescription = useRef('');
  const startDate = useRef('');
  const durationInMin = useRef('');
  const maxStudentCount = useRef('');
  const minStudentCount = useRef('');
  const price = useRef('');
  var m = new Date();
  // const dateString = m.getUTCDate() +"."+ m.getUTCMonth() +"."+ m.getUTCFullYear();
  const dateString = "11.06.2021";


  const Submit = async (e) => {
    e.preventDefault();
    const data = {
      "teacherId": userId,
      "lessonTitle": lessonTitle.current.value,
      "lessonDescription": lessonDescription.current.value,
      "startDate": startDate.current.value,
      "durationInMin": durationInMin.current.value,
      "maxStudentCount": maxStudentCount.current.value,
      "minStudentCount": minStudentCount.current.value,
      "price": price.current.value
    };

    const createLessonRequest = async () => {
      console.log(data);
      const response = await LessonServiceAPI.post('/api/v1/Lessons', data);
      var content = await response.data.content;
      console.log(response.status);

      if (response.status = 201) {
        setOpenSnack(true);
      }
      getTeacherLessons();
    }

    createLessonRequest();


    setOpenDialog(false);
    //fetchRequest();
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <Divider />
        <List><TeacherSideBar /></List>
        <Divider />
      </Drawer>



      <main className={classes.content}>
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={1}>
            <Button size="small" color="secondary" variant="contained" onClick={handleClickOpen}>Создать урок</Button>

            <Dialog open={openDialog} onClose={handleClose} aria-labelledby="form-dialog-title">
              <form onSubmit={Submit}>
                <DialogTitle id="form-dialog-title">Создание урока</DialogTitle>
                <DialogContent>

                  <TextField
                    autoFocus
                    margin="dense"
                    id="lessonTitle"
                    label="Тема урока"
                    type="text"
                    fullWidth
                    inputRef={lessonTitle}
                  />

                  <TextField
                    margin="dense"
                    id="lessonDescription"
                    label="Описание"
                    type="textarea"
                    fullWidth
                    multiline
                    rows={5}
                    inputRef={lessonDescription}
                  />

                  <TextField
                    id="startDate"
                    label="Дата начала"
                    type="date"
                    defaultValue={dateString}
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputRef={startDate}
                  />

                  <TextField
                    autoFocus
                    margin="dense"
                    id="durationInMin"
                    label="Продолжительность урока (мин)"
                    type="text"
                    fullWidth
                    inputRef={durationInMin}
                  />

                  <TextField
                    margin="dense"
                    id="maxStudentCount"
                    label="Максимальное количество студентов"
                    type="text"
                    fullWidth
                    inputRef={maxStudentCount}
                  />

                  <TextField
                    margin="dense"
                    id="minStudentCount"
                    label="Минимальное количество студентов"
                    type="text"
                    fullWidth
                    inputRef={minStudentCount}
                  />

                  <TextField
                    margin="dense"
                    id="price"
                    label="Цена"
                    type="text"
                    fullWidth
                    inputRef={price}
                  />

                </DialogContent>
                <DialogActions>
                  <Button onClick={handleClose} color="primary">Отмена</Button>
                  <Button type="submit" color="primary">Создать</Button>
                </DialogActions>
              </form>
            </Dialog>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                {teacherLessons.map(lesson => (
                  <Grid item xs={12}>
                    <Grid item xs={12}>
                      <Lesson lesson={lesson} />
                    </Grid>
                    <Grid container xs={12}>
                      <Grid item xs={3} className={classes.buttonContainer}>
                        <Button
                          variant="contained"
                          color="primary"
                          size="large"
                          fullWidth
                          className={classes.button}
                          startIcon={<PlayArrow />}
                        >
                          Начать урок
                        </Button>
                      </Grid>
                      <Grid item xs={3}  className={classes.buttonContainer}>
                        <Button
                          variant="contained"
                          color="Secondary"
                          size="large"
                          fullWidth
                          className={classes.button}
                          startIcon={<StopIcon />}
                        >
                          Остановить урок
                        </Button>
                      </Grid>
                      <Grid item xs={3}  className={classes.buttonContainer}>
                        <Button
                          variant="outlined"
                          color="Default"
                          size="large"
                          fullWidth
                          className={classes.button}
                          startIcon={<HighlightOff />}
                        >
                          Отменить урок
                        </Button>
                      </Grid>

                    </Grid>
                  </Grid>
                ))}

              </Paper>
            </Grid>
          </Grid>
        </Container>
        <CustomizedSnackbars open={openSnack} onClose={handleSnackClose} message={"Урок успешно создан"} />
      </main>
    </div>
  );
}