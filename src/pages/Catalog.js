import { Container, Divider } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { ProfileServiceAPI } from '../API/API';
import Teacher from '../components/Teacher';
import { useSelector, useDispatch } from 'react-redux';

function Catalog() {
  const [AllTeachers, setAllTeachers] = useState([]);
  const jwt = useSelector(state => state.jwt);
  const userId = useSelector(state => state.userId);
  const userRole = useSelector(state => state.userRole);

  useEffect(() => {
    getAllTeachers();
  }, []);

  const getAllTeachers = async () => {
    const response = await ProfileServiceAPI.get('api/v1/teacher');
    var content = await response.data.data;   
    setAllTeachers(content);
  }

  return (  
    <>
      <Container maxWidth="sm">
      {AllTeachers.map(teacher => (
          <Teacher teacher={teacher} />
        ))}
      </Container>
    </>
  );
};

export default Catalog;