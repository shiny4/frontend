import React from 'react';
import {Paper, Container, Typography, Grid, Button, CardMedia, CardContent, Card, CardActions, Box, Divider} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import { Redirect } from 'react-router';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow:1
	},
	mainFeaturePost:{
		position: 'relative',
		color: theme.palette.common.white,
		marginBottom: theme.spacing(4),
		backgroundSize: "cover",
		backgroundRepeat: "no-repeat",
		backgroundPosition: "center"
	},
	mainFeaturePostContent:{
		position: 'relative',
		padding: theme.spacing(9),

	},
	overlay:{
		position: 'absolute',
		top:0,
		bottom:0,
		left: 0,
		right: 0,
		background: "rgba(0,0,0,.6)"

	},
	cardMedia: {
		paddingTop: "56.25%"
	},
	cardContent:{
		flexGrow: 1
	},
	teacherCard:{
		marginTop: theme.spacing(4)
	},
	footer: {
		height: '4vh',
		background: 'darkgrey',
		marginTop: theme.spacing(2),
		padding: theme.spacing(4)
	},
	closestOpenedLessonsSection: {
		background: 'lightgrey',
		marginTop: theme.spacing(2),
		paddingTop: theme.spacing(4)
	}

})) 

const cardsTeacher = [1,2,3,4,5,6,7,8,9];
const openedLessons = [1,2,3,4,5,6,7,8];



const handleCatalogAction = () => {

	return <Redirect to="/catalog" />
}





function Home(){
	const classes = useStyles();

    return (
        <>
			<main>
				<Paper className={classes.mainFeaturePost} 
				style ={{ backgroundImage: `url(https://source.unsplash.com/1600x900/?science)`}}>
					<Container fixed>
						<div className={classes.overlay}/>
						<Grid container>
							<Grid item md={6} >
								<div className={classes.mainFeaturePostContent}>
									<Typography
										component = "h1"
										variant="h4"
										color = "inherit"
										gutterBottom
									>
										JUMPING STUDY SYSTEM
									</Typography>
									<Typography
										component = "h6"
										color = "inherit"
										paragraph
									>
										Id cillum duis minim in eu aliquip elit eu id id ipsum. Nostrud culpa ad consectetur qui nisi. Eiusmod minim tempor ut cillum labore occaecat culpa incididunt consectetur. Exercitation id aliquip commodo voluptate sunt qui commodo do magna excepteur ipsum. Dolore laborum esse tempor labore. Ipsum mollit occaecat esse est. Ullamco ad incididunt cillum aute ad officia anim nostrud.
									</Typography>
									<Button variant="contained" color="secondary">СОЗДАТЬ ЗАЯВКУ</Button>
								</div>
							</Grid>
						</Grid>
					</Container>
				</Paper>

				<div className={classes.mainContent}>
				<Container maxWidth="md">
					<Typography
					variant ="h4"
					align="center"
					color="textPrimary"
					gutterBottom
					>
						ЛУЧШИЕ ПРЕПОДАВАТЕЛИ
					</Typography>
				</Container>
				<Container maxWidth="md">
					<Typography
					variant ="h6"
					align="center"
					color="textSecondary"
					paragraph
					>
						Id incididunt cupidatat enim exercitation elit sint minim ullamco qui aute aliquip occaecat qui. Dolor cillum enim pariatur velit. Est velit magna officia et sint adipisicing consectetur fugiat incididunt exercitation. Ipsum non dolore nisi cillum officia labore aliqua officia nulla sint id excepteur.
					</Typography>
					<div className={classes.manButtons}>
						<Grid container spacing={2} justify="center">
							<Grid item>
								<Button color="primary" variant="contained">Поиск преподавателя</Button>
							</Grid>
							<Grid item>
								<Button color="primary" variant="outlined" onClick={handleCatalogAction}>Весь список</Button>
							</Grid>
						</Grid>
					</div>
				</Container>
			</div>
				<Container className={classes.teacherCard} maxWidth="md">
				<Grid container spacing={4}>
					{cardsTeacher.map((card) => (
						<Grid item key={card} xs="12" sm="6" md={4}>
							<Card className={classes.card}>
								<CardMedia
									className={classes.cardMedia}
									image = "https://source.unsplash.com/random"
									title = "Image Title"
								/>
								<CardContent className ={classes.cardContent}>
								<Box component="fieldset" mb={3} borderColor="transparent">
									<Typography component="legend"></Typography>
									<Rating name="read-only" value={3.8} readOnly />
								</Box>
									<Typography variant = "h6"	gutterBottom>
										ФИО преподавателя
									</Typography>
									<Typography variant = "p" color="textSecondary">
										Описание скилов преподавателя. Описание скилов преподавателя. Описание скилов преподавателя
									</Typography>
								</CardContent>
								<CardActions>
									<Button size="small" color="primary">Просмотреть</Button>
									<Button size="small" color="secondary" variant="contained" style={{ flex: 1 }}>Подать заявку</Button>
								</CardActions>
								
							</Card>
						</Grid>
					))}
				</Grid>
			</Container>

			<div className={classes.closestOpenedLessonsSection}>
				<Container maxWidth="md">
					<Typography
					variant ="h4"
					align="center"
					color="textPrimary"
					gutterBottom
					>
						БЛИЖАЙШИЕ ОТКРЫТЫЕ УРОКИ
					</Typography>
				</Container>
				<Container maxWidth="md">
					<Typography
					variant ="h6"
					align="center"
					color="textSecondary"
					paragraph
					>
						Id incididunt cupidatat enim exercitation elit sint minim ullamco qui aute aliquip occaecat qui. Dolor cillum enim pariatur velit. Est velit magna officia et sint adipisicing consectetur fugiat incididunt exercitation. Ipsum non dolore nisi cillum officia labore aliqua officia nulla sint id excepteur.
					</Typography>
					<div className={classes.manButtons}>
						<Grid container spacing={2} justify="center">
							<Grid item>
								<Button color="primary" variant="outlined">Весь список</Button>
							</Grid>
						</Grid>
					</div>
				</Container>
				<Container className={classes.teacherCard} maxWidth="lg">
				<Grid container spacing={4}>
					{openedLessons.map((card) => (
						<Grid item key={card} xs={12} sm={6} md={3}>
							<Card className={classes.card}>
								<CardContent className ={classes.cardContent}>
									<Typography variant = "h6"	gutterBottom>
										Тема урока
									</Typography>
									<Typography	component="h5" gutterBottom>
										Преподаватель: ФИО
									</Typography>
									<Typography variant = "p" component="p" color="textSecondary" marginBottom={2} paragraph>
										Описание урока. Описание урока. Описание урока.
									</Typography>
									<Divider/>
									<Typography paragraph></Typography>
									<Typography variant = "p"	marginTop='16px'>
										Дата: 25.05.2021 20:00 МСК
									</Typography>
								</CardContent>
								<CardActions>
									<Button size="small" color="primary">Просмотреть</Button>
									<Button size="small" color="secondary" variant="contained" style={{ flex: 1 }}>Записаться</Button>
								</CardActions>
								
							</Card>
						</Grid>
					))}
				</Grid>
				</Container>
			</div>

			</main>

			<footer className={classes.footer}>
				

				<Typography variant="body2" color="textSecondary" align="center" >
				{'Copyright © '}
					Jumping
				{' '}
				{new Date().getFullYear()}
				{'.'}
				</Typography>
			</footer>
        </>
    );
};

export default Home;