export const setJwtToken = (str) => {
    return {
        type: 'SETJWTTOKEN',
        payload: str
    }
}

export const resetJwtToken = () => {
    return {
        type: 'RESETJWTTOKEN'
    }
}

export const setUserId = (str) => {
    return {
        type: 'SET_USER_ID',
        payload: str
    }
}

export const resetUserId = () => {
    return {
        type: 'RESET_USER_ID'
    }
}

export const setUserRole = (str) => {
    return {
        type: 'SET_USER_ROLE',
        payload: str
    }
}

export const resetUserRole = () => {
    return {
        type: 'RESET_USER_ROLE'
    }
}