const jwtReducer = (state = '', action) => {
    switch (action.type) {
        case 'SETJWTTOKEN':
            state = action.payload;
            return state;
        case 'RESETJWTTOKEN':
            state = '';
            return state;
        default:
            return state;
    }
}

export default jwtReducer;