const userIdReducer = (state = '', action) => {
    switch (action.type) {
        case 'SET_USER_ID':
            state = action.payload;
            return state;
        case 'RESET_USER_ID':
            state = '';
            return state;
        default:
            return state;
    }
}

export default userIdReducer;