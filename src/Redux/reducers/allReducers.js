import jwtReducer from './jwtReducer';
import userIdReducer from './userIdReducer';
import userRoleReducer from './userRoleReducer';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
    jwt: jwtReducer,
    userId: userIdReducer,
    userRole: userRoleReducer
})

export default allReducers;