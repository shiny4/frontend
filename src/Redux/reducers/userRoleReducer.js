const userRoleReducer = (state = '', action) => {
    switch (action.type) {
        case 'SET_USER_ROLE':
            state = action.payload;
            return state;
        case 'RESET_USER_ROLE':
            state = '';
            return state;
        default:
            return state;
    }
}

export default userRoleReducer;