import axios from "axios";

// For common config
axios.defaults.headers.post["Content-Type"] = "application/json";

const ApplicationServiceAPI = axios.create({
    baseURL: 'http://localhost:8100/'
});

const ProfileServiceAPI = axios.create({
    baseURL: 'http://localhost:8100/'
});

const LessonServiceAPI = axios.create({
    baseURL: 'http://localhost:8100/'
});

const NotificationServiceAPI = axios.create({
    baseURL: 'https://some-custom-domain.com/api/'
});

export {
    ApplicationServiceAPI,
    ProfileServiceAPI,
    LessonServiceAPI,
    NotificationServiceAPI
};